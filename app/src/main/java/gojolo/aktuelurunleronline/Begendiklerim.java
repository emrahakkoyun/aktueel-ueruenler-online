package gojolo.aktuelurunleronline;


import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class Begendiklerim extends Fragment {
    View view;
    GridView grid;
    String[] gimgs=null;
    Fragment fragment=new ShowFragment();
    FirebaseDatabase db;
    String deviceId;
    ArrayList<String> list=new ArrayList<String>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_begendiklerim, container, false);
        grid = (GridView)view.findViewById(R.id.grid);
        db=FirebaseDatabase.getInstance();
        deviceId= Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID);
       yazicek();
        return view;
    }
    private void showbegendim() {
        grid.setAdapter(new KatalogAdapter(getActivity(), gimgs));
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                loadFragment(fragment,position,gimgs[position]);
            }
        });
    }
    public void yazicek(){
        DatabaseReference dbRefYeni =db.getReference("user/"+deviceId);
        dbRefYeni.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> keys=dataSnapshot.getChildren();
                for(DataSnapshot key:keys)
                {
                    list.add(key.getValue().toString());
                    Log.i("veriler",key.getValue().toString());
                }
                gimgs = new String[list.size()];
                gimgs=list.toArray(gimgs);
                showbegendim();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void loadFragment(Fragment fragment,int position,String link) {
        // load fragment
        Bundle bundle = new Bundle();
        bundle.putInt("gelen", position);
        bundle.putString("img", link);
        fragment.setArguments(bundle);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}