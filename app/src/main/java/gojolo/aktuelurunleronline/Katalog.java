package gojolo.aktuelurunleronline;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class Katalog extends Fragment {
    View view;
    GridView grid;
    String link;
    String[] gimgs=null;
    String[] glinks=null;
    Fragment fragment=new ShowFragment();
    private InterstitialAd mInterstitial;
    //Boolean variable to mark if the transaction is safe
    private boolean isTransactionSafe;

    //Boolean variable to mark if there is any transaction pending
    private boolean isTransactionPending;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_katalog, container, false);
        grid = (GridView)view.findViewById(R.id.grid);
        // Create the InterstitialAd and set the adUnitId (defined in values/strings.xml).
        MobileAds.initialize(getActivity(),
                "ca-app-pub-1312048647642571~8020515589");
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            int i = bundle.getInt("gelen", 0);
            link = bundle.getString("link", "");
            getWebsite();
        }
// get the reference of Button
        return view;
    }
    private void getWebsite() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final StringBuilder builder = new StringBuilder();

                try {
                    Document doc = Jsoup.connect(link).get();
                    Elements links = doc.select(".gallery-item a[href]");
                    Elements imgs = doc.select(".gallery-item img[src]");
                    glinks = new String[links.size()];
                    int i=0;
                    for (Element linx: links)
                        {
                            glinks[i] = linx.attr("href");
                            i++;
                        }
                        i=0;
                        gimgs = new String[imgs.size()];
                        for (Element img : imgs)
                            {
                                gimgs[i] = img.attr("src");
                                Log.i("href", img.attr("src"));
                                i++;
                          }
                } catch (IOException e) {
                    builder.append("Error : ").append(e.getMessage()).append("\n");
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        grid.setAdapter(new KatalogAdapter(getActivity(), gimgs));
                        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view,
                                                    int position, long id) {
                                inrequestadd(fragment,position,glinks[position]);
                            }
                        });
                    }
                });
            }
        }).start();
    }
    public void inrequestadd(Fragment fragmentx,int positionx,String linkx) {
        final Fragment fragment;
        final int position;
        final String link;
        fragment=fragmentx;
        position=positionx;
        link=linkx;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
       final  SharedPreferences.Editor editor = preferences.edit();
        int myInt = preferences.getInt("reklam", -1);
        if(myInt==1)
        {
            loadFragment(fragment,position,link);
        }
        else {
            mInterstitial = new InterstitialAd(getActivity());
            mInterstitial.setAdUnitId("ca-app-pub-1312048647642571/6648009575");
            mInterstitial.loadAd(new AdRequest.Builder().build());
            mInterstitial.setAdListener(new AdListener() {

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    loadFragment(fragment, position, link);
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    super.onAdFailedToLoad(errorCode);
                    loadFragment(fragment, position, glinks[position]);
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    if (mInterstitial.isLoaded()) {
                        mInterstitial.show();
                        editor.putInt("reklam", 1);
                        editor.commit();
                    } else {
                        loadFragment(fragment, position, glinks[position]);
                    }
                }

            });
        }
    }
/*
onPause is called just before the activity moves to background and also before onSaveInstanceState. In this
we will mark the transaction as unsafe
 */
public void onResume(){
    super.onResume();
    isTransactionSafe=true;
}
    public void onPause(){
        super.onPause();
        isTransactionSafe=false;

    }
    private void loadFragment(Fragment fragment,int position,String link) {
        // load fragment
        Bundle bundle = new Bundle();
        bundle.putInt("gelen", position);
        bundle.putString("link", link);
        if(isTransactionSafe) {
            fragment.setArguments(bundle);
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        //   super.onSaveInstanceState(outState);
    }
}