package gojolo.aktuelurunleronline;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class KatalogAdapter extends ArrayAdapter {
    private Context context;
    private LayoutInflater inflater;

    private String[] imageUrls;

    public KatalogAdapter(Context context, String[] imageUrls) {
        super(context, R.layout.listview_item_image, imageUrls);

        this.context = context;
        this.imageUrls = imageUrls;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (null == convertView) {
            convertView = (View)inflater.inflate(R.layout.listview_item_image, parent, false);
        }
        if (imageUrls[position].isEmpty()) {

        }
        else {
            Picasso
                    .with(context)
                    .load(imageUrls[position])
                  .fit() // will explain later
                    .into((ImageView) convertView);
        }
        return convertView;
    }
}