package gojolo.aktuelurunleronline;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.io.IOException;

import java.io.IOException;

public class Mainfragment extends Fragment {
    String oldbaslik;
    String[] yazilar=new String[7];
    String[]  linkler=new String[7];
    Integer[] resimler={
            R.drawable.r1,
            R.drawable.r2,
            R.drawable.r3,
            R.drawable.r4,
            R.drawable.r5,
            R.drawable.r6,
            R.drawable.r7,
            R.drawable.r8
    };
    String[] veriler={
            "https://www.aktuelurunkampanya.com/kampanyalar/bim-aktuel",
            "https://www.aktuelurunkampanya.com/kampanyalar/a101-aktuel",
            "https://www.aktuelurunkampanya.com/kampanyalar/sok-aktuel",
            "https://www.aktuelurunkampanya.com/kampanyalar/hakmar-aktuel",
            "https://www.aktuelurunkampanya.com/kampanyalar/migros-kampanyalari",
            "https://www.aktuelurunkampanya.com/kampanyalar/kipa-kampanyalari",
            "https://www.aktuelurunkampanya.com/kampanyalar/carrefour-kampanyalari",
            "https://www.aktuelurunkampanya.com/kampanyalar/market-kampanyalari/gratis-kampanyalari"
    };
    View view;
    FirebaseDatabase db;
    Fragment fragment=new Katalog();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
// Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_mainfragment, container, false);
        db = FirebaseDatabase.getInstance();
        getWebsite();
// get the reference of Button
        return view;
    }
    private void getWebsite() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final StringBuilder builder = new StringBuilder();

                try {
                    for(int i=0;i<veriler.length-1;i++){
                        Document doc = Jsoup.connect(veriler[i]).get();
                       String baslik= doc.select(".title").first().text();
                        yazilar[i]=baslik;
                        String link = doc.select(".title a[href]").first().attr("href");
                        linkler[i]=link;
                        yazitest(baslik,i);
                    }
                    //A101

                } catch (IOException e) {
                    builder.append("Error : ").append(e.getMessage()).append("\n");
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ImageAdapter adapter = new ImageAdapter(getActivity(), yazilar, resimler);
                        GridView grid=(GridView)view.findViewById(R.id.grid);
                        grid.setAdapter(adapter);
                        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view,
                                                    int position, long id) {
                                loadFragment(fragment,position,linkler[position]);
                            }
                        });
                    }
                });
            }
        }).start();
    }
    private void loadFragment(Fragment fragment,int position,String link) {
        // load fragment
        Bundle bundle = new Bundle();
        bundle.putInt("gelen", position);
        bundle.putString("link", link);
        fragment.setArguments(bundle);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    public void  yazitest(final String baslik,final int i){
        DatabaseReference dbRef0=db.getReference("yazilar").child("0");
        DatabaseReference dbRef1=db.getReference("yazilar").child("1");
        DatabaseReference dbRef2=db.getReference("yazilar").child("2");
        DatabaseReference dbRef3=db.getReference("yazilar").child("3");
        DatabaseReference dbRef4=db.getReference("yazilar").child("4");
        DatabaseReference dbRef5=db.getReference("yazilar").child("5");
        DatabaseReference dbRef6=db.getReference("yazilar").child("6");
        switch (i) {
            case 0: {
                dbRef0.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getValue()!=null) {
                        oldbaslik=dataSnapshot.getValue().toString();
                        if(oldbaslik!=baslik){
                            yaziekle(baslik,i);
                        }}
                        else{
                            yaziekle(baslik,i);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                break;
            }
            case 1: {
                dbRef1.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getValue()!=null) {
                            oldbaslik=dataSnapshot.getValue().toString();
                            if(oldbaslik!=baslik){
                                yaziekle(baslik,i);
                            }}
                        else{
                            yaziekle(baslik,i);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                break;
            }
            case 2: {
                dbRef2.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getValue()!=null) {
                            oldbaslik=dataSnapshot.getValue().toString();
                            if(oldbaslik!=baslik){
                                yaziekle(baslik,i);
                            }}
                        else{
                            yaziekle(baslik,i);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                break;
            }
            case 3: {
                dbRef3.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getValue()!=null) {
                            oldbaslik=dataSnapshot.getValue().toString();
                            if(oldbaslik!=baslik){
                                yaziekle(baslik,i);
                            }}
                        else{
                            yaziekle(baslik,i);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                break;
            }
            case 4: {
                dbRef4.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getValue()!=null) {
                            oldbaslik=dataSnapshot.getValue().toString();
                            if(oldbaslik!=baslik){
                                yaziekle(baslik,i);
                            }}
                        else{
                            yaziekle(baslik,i);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                break;
            }
            case 5: {
                dbRef5.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getValue()!=null) {
                            oldbaslik=dataSnapshot.getValue().toString();
                            if(oldbaslik!=baslik){
                                yaziekle(baslik,i);
                            }}
                        else{
                            yaziekle(baslik,i);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                break;
            }
            case 6: {
                dbRef6.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getValue()!=null) {
                            oldbaslik=dataSnapshot.getValue().toString();
                            if(oldbaslik!=baslik){
                                yaziekle(baslik,i);
                            }}
                        else{
                            yaziekle(baslik,i);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                break;
            }
        }
    }
    public void  yaziekle(String yazilar,int i){
        DatabaseReference dbRef=db.getReference("yazilar");
        DatabaseReference dbRefYeni=db.getReference("yazilar/"+i);
        dbRefYeni.setValue(yazilar);

    }
}