package gojolo.aktuelurunleronline;


import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ShareActionProvider;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;
import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import static android.provider.Telephony.Mms.Part.TEXT;


public class ShowFragment extends Fragment {
    View view;
    String link;
    PhotoView img;
    String imgs;
    String imgx;
    String deviceId;
    PhotoViewAttacher photoViewAttacher;
    FirebaseDatabase db;
    MenuItem likeitem;
    boolean tf=true;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_show, container, false);
        img = (PhotoView) view.findViewById(R.id.img);
        db = FirebaseDatabase.getInstance();
       deviceId= Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            link = bundle.getString("link", "0");
            imgx= bundle.getString("img", "0");
            if(imgx!="0"&&link=="0")
            {
             begendigimshow();
            }
            else{getWebsite();}
        }
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.share);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareApp(getActivity());
            }
        });
// get the reference of Button
        return view;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.main, menu);
        likeitem = menu.findItem(R.id.likes);
        super.onCreateOptionsMenu(menu, inflater);
    }
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.likes) {
            if(imgs!=null)
            resimekle(imgs);
            if(tf==true)
            { likeitem.setIcon(R.drawable.ic_thumb_up_blackm_24dp);}
            else { tf=false;
                likeitem.setIcon(R.drawable.ic_thumb_up_black0_24dp);}
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void getWebsite() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final StringBuilder builder = new StringBuilder();

                try {
                    Document doc = Jsoup.connect(link).get();
                    Element element = doc.select("#image img[src]").first();
                    imgs = element.attr("src");
                    Log.i("img", imgs);

                } catch (IOException e) {
                    builder.append("Error : ").append(e.getMessage()).append("\n");
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Picasso.with(getActivity().getApplicationContext())
                                .load(imgs)
                                .into(img);
                        photoViewAttacher = new PhotoViewAttacher(img);
                    }
                });
            }
        }).start();
    }
    public void  begendigimshow(){
        Picasso.with(getActivity().getApplicationContext())
                .load(imgx)
                .into(img);
        photoViewAttacher = new PhotoViewAttacher(img);
    }
    private static void shareApp(Context context) {
        final String appPackageName = BuildConfig.APPLICATION_ID;
        final String appName = context.getString(R.string.app_name);
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        String shareBodyText = "https://play.google.com/store/apps/details?id=" +
                appPackageName;
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, appName);
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareBodyText);
        context.startActivity(Intent.createChooser(shareIntent, context.getString(R.string
                .share_with)));
    }
    public void  resimekle(String resim){
        DatabaseReference dbRef=db.getReference("user");
        DatabaseReference dbRefYeni=db.getReference("user/"+deviceId);
        String randblok= dbRefYeni.push().getKey();
        DatabaseReference dbRefYeniblok=db.getReference("user/"+deviceId+"/"+randblok);
        dbRefYeniblok.setValue(resim);

    }
}